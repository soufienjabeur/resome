import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NavbarService } from '../services/navbar.service';
import { ApiService } from '../services/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-session',
  templateUrl: './admin-session.component.html',
  styleUrls: ['./admin-session.component.css']
})
export class AdminSessionComponent implements OnInit {

  adminFormGroup: FormGroup;

  // tslint:disable-next-line: max-line-length
  constructor( public navbarservice: NavbarService, public formBuilder: FormBuilder, private apiservice: ApiService, private router: Router) {
    this.navbarservice.hide();
    this.intForm();
   }

  ngOnInit() {

  }

  intForm() {
    this.adminFormGroup = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }
  onSubmit() {

  }

}
