import { Component, OnInit } from '@angular/core';
import { NavbarService } from 'src/app/services/navbar.service';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-list-students',
  templateUrl: './list-students.component.html',
  styleUrls: ['./list-students.component.css']
})
export class ListStudentsComponent implements OnInit {
  displayedColumns = ['Nom', 'Prénom', 'email', 'password'];
  Students: any = [];

  constructor(public navbarservice: NavbarService, private apiservice: ApiService) {
    this.navbarservice.hide();
   }
  readEmploye() {
    this.apiservice.getStudents().subscribe((data) => {
      this.Students = data;
    });

  }
  ngOnInit() {
  }

}
