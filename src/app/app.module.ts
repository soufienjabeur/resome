import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './header/header.component';
// tslint:disable-next-line: max-line-length
import {MatTableModule , MatToolbarModule, MatIconModule, MatMenuModule, MatButtonModule, MatGridListModule, MatCardModule, MatSelectModule, MatOptionModule, MatInputModule, MatSidenavModule} from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import { SigninComponent } from './auth/signin/signin.component';
import { SignupComponent } from './auth/signup/signup.component';
import { BodyComponent } from './body/body.component';
import { AboutComponent } from './body/about/about.component';
import { FooterComponent } from './footer/footer.component';
import { ContactComponent } from './body/contact/contact.component';
import { ApiService } from './services/api.service';
import {HttpClientModule} from '@angular/common/http';
import { StudentSessionComponent } from './body/student-session/student-session.component';
import { AdminSessionComponent } from './admin-session/admin-session.component';
import { ListStudentsComponent } from './admin-session/list-students/list-students.component';





@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SigninComponent,
    SignupComponent,
    BodyComponent,
    AboutComponent,
    FooterComponent,
    ContactComponent,
    StudentSessionComponent,
    AdminSessionComponent,
    ListStudentsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatGridListModule,
    MatFormFieldModule,
    MatCardModule,
    MatSelectModule,
    MatOptionModule,
    MatInputModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    MatSidenavModule,
    MatTableModule

  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
