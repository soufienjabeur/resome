import { Injectable } from '@angular/core';
import {HttpHeaders, HttpClient, HttpErrorResponse} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import {catchError} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  baseUrl = 'http://localhost:4000/api';
  headers = new HttpHeaders().set('Content-Type', 'application-json');

  constructor(private http: HttpClient) { }

  // Create a student
  createStudent(data): Observable<any> {
    let url = `${this.baseUrl}/create`;
    return this.http.post(url, data).pipe( catchError(this.errorMgmt));
  }

  // get All the student

  getStudents() {
    return this.http.get(`${this.baseUrl}`);
  }

  // tslint:disable-next-line: no-shadowed-variable
  errorMgmt( error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code :${error.status}\nMessage:${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
   }

}

