import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SigninComponent } from './auth/signin/signin.component';
import { SignupComponent } from './auth/signup/signup.component';


import { AboutComponent } from './body/about/about.component';
import { ContactComponent } from './body/contact/contact.component';
import { StudentSessionComponent } from './body/student-session/student-session.component';
import { AdminSessionComponent } from './admin-session/admin-session.component';
import { ListStudentsComponent } from './admin-session/list-students/list-students.component';

const routes: Routes = [
  {path: 'auth/sigin', component: SigninComponent},
  {path: 'auth/signup', component: SignupComponent},
  {path: 'about', component: AboutComponent},
  {path: 'contact', component: ContactComponent},
  {path: 'studentSession', component: StudentSessionComponent},
  {path : 'adminSession', component: AdminSessionComponent},
  {path: 'view', component: ListStudentsComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
