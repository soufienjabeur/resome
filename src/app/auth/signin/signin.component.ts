import { Component, OnInit, NgZone } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  submitted = false;
  studentFormGroup: FormGroup;

  // tslint:disable-next-line: max-line-length
  constructor(public formbuilder: FormBuilder, private apiservice: ApiService, private router: Router, private ngZone: NgZone, private activerouter: ActivatedRoute
  ) {
    this.subscribeForm();
    console.log(activerouter.snapshot.url[1].path);
   }
  hide = true;


  ngOnInit() {
  }


  subscribeForm() {
    this.studentFormGroup = this.formbuilder.group({
      nom: ['', [Validators.required]],
      prenom: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      password: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{8,}/)]]
    });
  }

  // Getter to access form control
  get myForm(){
    return this.studentFormGroup.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (!this.studentFormGroup.valid) {
      return false;
    } else {
      this.apiservice.createStudent(this.studentFormGroup.value).subscribe(
        (res) => {
          console.log('Successfully  Student created');
          this.ngZone.run(() => this.router.navigateByUrl('/studentSession'));
        }, (error) => {
          console.log(error);
        }
      );
    }
  }
}
