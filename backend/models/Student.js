const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Define Collection and Schema

let Student = new Schema({
  nom :{
    type: String
  },
  prenom :{
    type: String
  },
  email :{
    type: String
  },
  password :{
    type: String
  },
},{
  collection:'students'
})
module.exports = mongoose.model('Student', Student);
