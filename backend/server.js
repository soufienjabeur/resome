let express = require('express');
path= require('path');
mongoose = require('mongoose');
cors= require('cors');
bodyParser = require('body-parser');
dbConfig =  require('./database/db');

//Connecting with mongo db

mongoose.Promise= global.Promise;
mongoose.connect(dbConfig.db, {
  userNewUrlParser:true
}).then(()=>{
  console.log('Database Successfully Connected');},
  error =>{
    console.log('Database could not connected ' +error );
  }
);

// Setting up port with express js
const studentRoute = require('./routes/student.route');
const app=express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cors());
app.use(express.static(path.join(__dirname,'dist/Resome')));
app.use('/',express.static(path.join(__dirname,'dist/Resome')));
app.use('/api', studentRoute);

//create port
const port = process.env.Port || 4000;
const server = app.listen(port, ()=>{
  console.log('Connected to port '+ port);
});

// Find 404 and hand over to error handler
app.use((req, res, next)=>{
  next(createError(404));
});

//error handler
app.use(function(err,req,res,next){
  //Log error message in our server's console
  console.error(err.message);
  //if error has no specified code, set error code to interval Server Error(500)
  if(!err.statusCode)err.statusCode=500;
  //All Http requests must have a response, so let's send back an error with its status code and message
  res.status(err.statusCode).send(err.message);
});
