const express= require('express');
const app = express();
const studentRoute = express.Router();

// Student model
let Student = require('../models/Student');


// Add Student
studentRoute.route('/create').post((req, res, next)=>{
  Student.create(req.body, (error, data)=>{
    if(error){
      return next(error);
    }
    else{
      res.json(data);
    }
  });
});

//Get All Student
studentRoute.route('/').get((req, res)=>{
  Student.find((error, data)=>{
    if(error){
      return next(error);
    }
    else{
      res.json(data);
    }
  });
});

// Get Single Student
studentRoute.route('/read/:id').get((req,res)=>{
  Student.findById(req.params.indexOf, (error,data)=>{
    if(error){
      return next(error);
    }
    else{
      res.json(data);
    }
  });
});

//Update Student
studentRoute.route('/update/:id').put((req,res,next)=>{
Student.findByIdAndUpdate(req.params.id,{
  $set:req.body},(error,data)=>{
    if(error){
      return next(error);
      console.log(error);
    }
    else{
      res.json(data);
      console.log('Data update Successfully');
    }
  });
});

//Delete Student

studentRoute.route('/delete/:id').delete((req,res,next)=>{
Student.findByIdAndRemove(req.params.id,(error,data)=>{
  if(error){
    console.log(error);

  }
  else{
    res.status(200).json({
      msg:data
    });
  }
});
});

module.exports= studentRoute;
